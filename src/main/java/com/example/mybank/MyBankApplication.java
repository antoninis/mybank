package com.example.mybank;

import com.example.mybank.Entity.Address;
import com.example.mybank.Entity.Users;
import com.example.mybank.Repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class MyBankApplication {

	@Autowired
	private UsersRepository usersRepository;

	public static void main(String[] args) {
		SpringApplication.run(MyBankApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	private void testJpaMethods(){
		Address address = new Address();
		address.setCity("Moscow");
		address.setStreet("Volzhskaya street 20");

		Users users = new Users();
		users.setName("Artem");
		users.setEmail("22@mail.ru");
		users.setAddress(address);
		usersRepository.save(users);

		usersRepository.findAll().forEach(it-> System.out.println(it));
	}
}
