package com.example.mybank.Controllers;

import com.example.mybank.Entity.Users;
import com.example.mybank.Repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class MainController {
    @Autowired
    private UsersRepository usersRepository;


    @RequestMapping("/")
    @ResponseBody
    public String welcome() {
        return "Welcome to Example.";
    }

    @GetMapping("/users")
    List<Users> all() {
        return usersRepository.findAll();
    }

    @PostMapping("/users")
    Users newUser(@RequestBody Users newUser) {
        return usersRepository.save(newUser);
    }

    @GetMapping("/users/{id}")
    Optional<Users> one(@PathVariable Long id) {

        return usersRepository.findById(id);
    }

    @PutMapping("/users/{id}")
    Users replaceUsers(@RequestBody Users newUser, @PathVariable Long id) {
        return usersRepository.findById(id).
                map(users -> {
                    users.setName(newUser.getName());
                    users.setEmail(newUser.getEmail());
                    return usersRepository.save(users);
                })
                .orElseGet(() -> {
                    newUser.setId(id);
                    return usersRepository.save(newUser);
                });
    }

    @DeleteMapping("/users/{id}")
    void deleteUsers(@PathVariable Long id) {
        usersRepository.deleteById(id);
    }
}